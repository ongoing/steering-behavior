﻿using Unity.Mathematics;
using UnityEngine;

namespace Himeki.DOTS.UnitySteeringLib
{
    public enum SteeringBehaviourId
    {
        None = 0,
        Idle = 2,
        Seek = 4,
        Arrival = 8,
        Flee = 16,
        Pursue = 16*16,
        Evade = 16*16*16*16,
        Follow,
        Flocking,
        Alignment,
        Cohesion,
        Separation,
        Avoidance
    }
}