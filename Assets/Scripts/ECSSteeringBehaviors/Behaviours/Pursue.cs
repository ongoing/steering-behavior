﻿using Unity.Mathematics;

namespace Himeki.DOTS.UnitySteeringLib
{
    public static class Pursue
    {

        public static float3 steer(float3 agentPos, float3 targetPos, float agentMaxSpeed, float3 agentVelocity, float3 targetVelocity, float deltaTime)
        {
            float anticipationFactor = 1f;

            float3 pursueTargetPos = targetPos + agentVelocity * anticipationFactor*deltaTime;

            float3 pursueTargetDistanceVector = pursueTargetPos - agentPos;
            
            float distance = math.length(pursueTargetDistanceVector);
            if (distance > 0.01f)
            {
                float speed = distance / 0.3f;
                float3 desiredVelocity = (pursueTargetDistanceVector)*speed;//math.normalize(pursueTargetDistanceVector) * agentMaxSpeed;

                float3 steering = desiredVelocity - agentVelocity;

                return steering;
            }

            return float3.zero;
        }

    }
}