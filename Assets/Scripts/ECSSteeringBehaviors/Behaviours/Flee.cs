﻿using Unity.Mathematics;
using Random = UnityEngine.Random;

namespace Himeki.DOTS.UnitySteeringLib
{
    public static class Flee
    {
        public static float3 steer(float3 agentPos, float3 targetPos, float agentMaxSpeed, float3 agentVelocity)
        {
            float safeFleeDistance = 1.7f; //Todo: Figure out where to set this
            float3 distanceVector = targetPos - agentPos;
            float distance = math.length(distanceVector);
            if (distance < safeFleeDistance)
            {
                float3 desiredVelocity = math.normalize(distanceVector) * agentMaxSpeed*2;
                float3 steering = desiredVelocity - agentVelocity;
                // var randomX = 0.1f;//Random.Range(0, 0.3f);
                return -steering; //+ new float3(randomX,randomX,randomX);
            }

            // return -agentVelocity;
            return float3.zero;
        }
    }
}
