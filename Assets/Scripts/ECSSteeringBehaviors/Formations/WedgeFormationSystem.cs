﻿using Himeki.DOTS.UnitySteeringLib;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

[UpdateBefore(typeof(FormationSystem))]
[UpdateAfter(typeof(LeaderSystem))]
public class WedgeFormationSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var distance = 1.5f;//GameManager.Instance.AgentDistance;
        var Xdistance = 1.3f;//GameManager.Instance.AgentDistance;
        var Ydistance = 4.8f;//GameManager.Instance.AgentDistance;
        var width = 5;
        float midX = (width - 1) / 2f;
        
        // var posArray = new NativeArray<float3>(100,Allocator.TempJob);
        //
        // Entities
        //     .WithAll<WedgeFormation>()
        //     .ForEach((Entity entity, SteeringAgentParameters agent) =>
        //     {
        //         
        //     }).Run();
        
        Entities
            .WithAll<WedgeFormation>()
           .ForEach((Entity entity,ref SteeringAgentParameters agent, ref Translation translation, in FormationPlacement formationIndex) =>
           {
               // var index = formationIndex.Index + 2;
               // var z = index / 2f * distance;
               // var x = -z + 2 * z * (index % 2);
               // var pos = new float3(x, 0, -z);
               // agent.LocalPosition = pos;
               // translation.Value = pos;

               // var index = formationIndex.Index;
               // var x = index % width - midX;
               // var z = index / width + 1;
               // var pos = new float3(x * Xdistance, 0, -z * Ydistance); 
               // agent.LocalPosition = pos;
               // translation.Value = pos;
               
               var index = formationIndex.Index;
               var x = index % width - midX;
               var z = index / width + 1;
               var pos = new float3(x * agent.Width, 0, -z * agent.Length); 
               agent.LocalPosition = pos;
               translation.Value = pos;

               EntityManager.RemoveComponent<WedgeFormation>(entity);
           })
            .WithStructuralChanges()
            .WithoutBurst()
            .Run();
            // .ScheduleParallel();
    }
}
