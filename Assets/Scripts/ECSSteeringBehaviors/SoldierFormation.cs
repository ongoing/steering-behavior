﻿using Himeki.DOTS.UnitySteeringLib;
using Unity.Entities;
using UnityEngine;
using Unity.Collections;
using Unity.Transforms;
using Unity.Mathematics;
using Random = UnityEngine.Random;

public class SoldierFormation : MonoBehaviour
{
    [Header("Leader")]
    // [SerializeField]
    // private FormationType m_FormationType = FormationType.Testudo;
    //
    // [SerializeField]
    // private LeaderComponent m_Leader;

    public int m_NumberOfSoldiers1 = 10;
    public int m_NumberOfSoldiers2 = 10;

    public GameObject m_SoldierPrefab0;
    public GameObject m_SoldierPrefab00;
    public GameObject m_SoldierPrefab1;
    public GameObject m_SoldierPrefab2;

    private float m_curLength;
        
    private void Start()
    {
        LeaderSystem.worldPoint = float3.zero;
        var world = World.DefaultGameObjectInjectionWorld;
        var entityManager = world.EntityManager;

        var settings = new GameObjectConversionSettings(world, 0);
        var entity0 = GameObjectConversionUtility.ConvertGameObjectHierarchy(m_SoldierPrefab0, settings);

        //leader
        var leaderEntity = entityManager.Instantiate(entity0);
        entityManager.AddComponent<Translation>(leaderEntity);
        entityManager.AddComponent<LocalToWorld>(leaderEntity);
        entityManager.AddComponent<Rotation>(leaderEntity);
        // entityManager.AddComponentData(leaderEntity, new Obstacle{radius = 1f});
        entityManager.AddComponentData(leaderEntity, new FormationLeader { Id = 1 ,speed = 7});
        entityManager.AddComponentData(leaderEntity, new Velocity { Value =float3.zero});
        
        //leader
        entity0 = GameObjectConversionUtility.ConvertGameObjectHierarchy(m_SoldierPrefab00, settings);
        var virtualEntity = entityManager.Instantiate(entity0);
        entityManager.AddComponent<Translation>(virtualEntity);
        entityManager.AddComponent<LocalToWorld>(virtualEntity);
        entityManager.AddComponent<Rotation>(virtualEntity);
        entityManager.AddComponentData(virtualEntity, new FormationLeader { Id = 2 ,speed = 7});
        entityManager.AddComponentData(virtualEntity, new Velocity { Value =float3.zero});

        CreateSoldier(m_SoldierPrefab1, 2,2,m_NumberOfSoldiers1,1);
        CreateSoldier(m_SoldierPrefab2, 3,6,m_NumberOfSoldiers2,-1);


        World.DefaultGameObjectInjectionWorld.EntityManager.DestroyEntity(entity0);
    }

    private void CreateSoldier(GameObject go,float w,float l,int count,int beginIndex = 0)
    {
        
        var world = World.DefaultGameObjectInjectionWorld;
        var entityManager = world.EntityManager;
        var length = 0f;
        for(int i = 0; i < count; i++)
        {
            var width = 5;
            float midX = (width - 1) / 2f;
            float midY = 2;
            var index = i;
            var x = index % width - midX;
            var z = index / width;
            if (beginIndex > 0)
            {
                z += 1;
            }

            var localPos = new float3(x * w, 0, z * l*beginIndex);
            // length = -z * l;
            var entity = entityManager.CreateEntity();
            entityManager.AddComponentData(entity, new CompanionLink { Companion = Instantiate(go)});
            // entityManager.AddComponent<WedgeFormation>(entity);
            entityManager.AddComponent<Rotation>(entity);
            entityManager.AddComponent<Translation>(entity);
            entityManager.AddComponent<LocalToWorld>(entity);
            entityManager.AddComponentData(entity, new Velocity { Value = 0 });
            entityManager.AddComponentData(entity, new SteeringAgentParameters()
            {
                behaviour = SteeringBehaviourId.Pursue,
                // WorldPosition = float3.zero,
                LocalPosition = localPos,
                LeaderVelocity = float3.zero,
                LeaderPosition = float3.zero,
                LeaderRotation = quaternion.identity,
                avoidObstacles = true,
                mass = 1,
                maxForce = 12+beginIndex*0.5f,
                maxSpeed = 12+beginIndex*0.5f,
                radius = 1f,
                Width = w,
                Length = l,
            });
            entityManager.AddComponentData(entity, new FormationSoldier() { LeaderId = 1 });
            entityManager.AddComponentData(entity, new FormationPlacement { Index =beginIndex + i});
            entityManager.AddComponentData(entity, new CompanionLink { Companion = Instantiate(go)});
        }
        m_curLength = length;

        // entities.Dispose();
        // World.DefaultGameObjectInjectionWorld.EntityManager.DestroyEntity(entity);

    }
}
