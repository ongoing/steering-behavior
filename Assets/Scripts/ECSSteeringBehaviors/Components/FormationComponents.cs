﻿using System;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Serialization;

[Serializable]
public struct FormationGroup : ISharedComponentData
{
    public int LeaderId;
}

public struct FormationSoldier : IComponentData
{
    public int LeaderId;
}

[Serializable]
public struct FormationLeader : IComponentData
{
    public int Id;
    public float speed;
    public float3 virtualPos;
}

[Serializable]
public struct FormationPlacement : IComponentData
{
    public int Index; 
    // public int Count;
}

public struct FormationInitTag : IComponentData
{
}

[Serializable]
public struct TestudoFormation : IComponentData { }
[Serializable]
public struct OrbFormation : IComponentData { }
[Serializable]
public struct WedgeFormation : IComponentData { }

[Serializable]
public struct SelectedComponent : IComponentData { }
