﻿using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Himeki.DOTS.UnitySteeringLib
{
    public struct SteeringAgentParameters : IComponentData
    {
        public SteeringBehaviourId behaviour;
        public float3 WorldPosition;
        public float3 LocalPosition;
        public float3 LeaderVelocity;
        public float3 LeaderPosition;

        public float Width;
        public float Length;
        // public float3 LeaderRotation;
        public quaternion LeaderRotation;
        public bool avoidObstacles;
        public float maxSpeed;
        public float maxForce;
        public float mass;
        public float radius;
    }
    
}