﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Himeki.DOTS.UnitySteeringLib
{
    [BurstCompile]
    struct UpdateEntityTranslationJob : IJobChunk
    {
        [ReadOnly] public float deltaTime;
        public ComponentTypeHandle<Translation> translationType;
        [ReadOnly] public ComponentTypeHandle<SteeringAgentParameters> steeringAgentParametersType;
        public ComponentTypeHandle<Rotation> rotationType;
        [ReadOnly] public ComponentTypeHandle<Velocity> velocityType;
        [ReadOnly] public ComponentTypeHandle<LocalToWorld> localToWorldType;


        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var chunkTranslations = chunk.GetNativeArray(translationType);
            var chunkAgentParam = chunk.GetNativeArray(steeringAgentParametersType);
            var chunkVelocities = chunk.GetNativeArray(velocityType);
            var chunkRotations = chunk.GetNativeArray(rotationType);
            var localToWorlds = chunk.GetNativeArray(localToWorldType);

            for (int i = 0; i < chunk.Count; i++)
            {
                var velocity = chunkVelocities[i];
                var speedSq = math.lengthsq(velocity.Value);
                var translation = chunkTranslations[i];
                if(speedSq > 0.01f)
                {
                    var localToWorld = localToWorlds[i];
                    var newPos = translation.Value + velocity.Value * deltaTime;
                    var rotation = chunkRotations[i];
                    chunkTranslations[i] = new Translation { Value = newPos };
                    // Debug.LogError(newPos);

                    if (math.length(velocity.Value) > 0.1f)
                    {
                        var newRotation = quaternion.LookRotation(velocity.Value, localToWorld.Up);
                        chunkRotations[i] = new Rotation { Value = newRotation };
                    }
                    // var agent = chunkAgentParam[i];
                    // var newQuaternion = math.slerp(rotation.Value,  agent.LeaderRotation, 10 * deltaTime);
                    // chunkRotations[i] = new Rotation { Value = newQuaternion };
                    Debug.DrawLine(newPos,newPos+(velocity.Value),Color.green);

                }
            }
        }
    }
}