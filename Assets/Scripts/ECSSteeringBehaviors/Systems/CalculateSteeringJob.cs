﻿using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Himeki.DOTS.UnitySteeringLib
{
    [BurstCompile]
    struct CalculateSteeringJob : IJobChunk
    {
        [ReadOnly] public float deltaTime;
        public ComponentTypeHandle<Translation> translationType;
        public ComponentTypeHandle<Velocity> velocityType;
        public ComponentTypeHandle<SteeringAgentParameters> steeringAgentParametersType;
        // [ReadOnly] public ComponentTypeHandle<TargetEntity> targetType;
        [ReadOnly] public ComponentDataFromEntity<LocalToWorld> localToWorldFromEntity;
        [ReadOnly] [NativeDisableContainerSafetyRestriction] public ComponentDataFromEntity<Velocity> velocityFromEntity;

        [ReadOnly] [DeallocateOnJobCompletion] public NativeArray<Translation> obstaclesTranslations;
        [ReadOnly] [DeallocateOnJobCompletion] public NativeArray<Obstacle> obstacles;

        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
        {
            var chunkTranslations = chunk.GetNativeArray(translationType);
            var chunkSteeringAgentParameters = chunk.GetNativeArray(steeringAgentParametersType);
            var chunkVelocities = chunk.GetNativeArray(velocityType);
            // var chunkTargets = chunk.GetNativeArray(targetType);

            int obstaclesAmount = obstaclesTranslations.Length;
            var obstaclesPositions = new NativeArray<float3>(obstaclesAmount, Allocator.Temp);
            var obstaclesRadii = new NativeArray<float>(obstaclesAmount, Allocator.Temp);

            for (int i = 0; i < obstaclesAmount; i++)
            {
                obstaclesPositions[i] = obstaclesTranslations[i].Value;
                obstaclesRadii[i] = obstacles[i].radius;
            }

            for (var i = 0; i < chunk.Count; i++)
            {
                var translation = chunkTranslations[i];
                var steeringAgentParams = chunkSteeringAgentParameters[i];
                // TargetEntity target = chunkTargets[i];
                var velocity = chunkVelocities[i];

                // if (target.entity != Entity.Null )//&& localToWorldFromEntity.Exists(target.entity))
                {
                    // LocalToWorld targetLocalToWorld = localToWorldFromEntity[target.entity];
                    // float3 targetPos = targetLocalToWorld.Value.c3.xyz;
                    // float3 targetForward = targetLocalToWorld.Forward;
                    // float3 targetVelocity = velocityFromEntity[target.entity].Value;
           
                    var targetPosition = math.mul(steeringAgentParams.LeaderRotation, steeringAgentParams.LocalPosition);
                    var targetPos = targetPosition + steeringAgentParams.LeaderPosition;
                    // var targetPos = steeringAgentParams.WorldPosition;
                    var targetForward = float3.zero;
                    var targetVelocity = steeringAgentParams.LeaderVelocity;
                    float3 steering = float3.zero;
                    switch(steeringAgentParams.behaviour)
                    {
                        case SteeringBehaviourId.Idle:
                            steering = Idle.steer();
                            break;
                        case SteeringBehaviourId.Seek:
                            steering = Seek.steer(translation.Value, targetPos, steeringAgentParams.maxSpeed, velocity.Value);
                            break;
                        case SteeringBehaviourId.Arrival:
                            steering = Arrival.steer(translation.Value, targetPos, steeringAgentParams.maxSpeed, velocity.Value);
                            break;
                        case SteeringBehaviourId.Pursue:
                            steering = Pursue.steer(translation.Value, targetPos, steeringAgentParams.maxSpeed, velocity.Value, targetVelocity,deltaTime);
                            break;
                        case SteeringBehaviourId.Flee:
                            steering = Flee.steer(translation.Value, targetPos, steeringAgentParams.maxSpeed, velocity.Value);
                            break;
                        case SteeringBehaviourId.Evade:
                            steering = Evade.steer(translation.Value, targetPos, steeringAgentParams.maxSpeed, velocity.Value, targetVelocity,deltaTime);
                            break;
                        case SteeringBehaviourId.Follow:
                            steering = Follow.steer(translation.Value, targetPos, targetForward, steeringAgentParams.maxSpeed, velocity.Value);
                            break;
                    }
       
                    //躲避Leader
                    // var flee = Flee.steer(translation.Value, steeringAgentParams.LeaderPosition, steeringAgentParams.maxSpeed,
                    //     velocity.Value);
                    // steering += flee;
                    
                    if (math.length(steering) > steeringAgentParams.maxForce)
                    {
                        steering = math.normalize(steering) * steeringAgentParams.maxForce;
                    }
                    //
                    // var leaderDir = math.forward(steeringAgentParams.LeaderRotation);
                    // var angle1 = Vector3.Angle(steering, leaderDir);
                    // if (angle1<45f)
                    // {
                    //     var angle = 90;
                    //     var offsetVec = new float3(steering.x * math.cos(angle)-steering.z*math.sin(angle),0,
                    //         steering.x*math.sin(angle)+steering.z*math.cos(angle));
                    //     // steering += math.normalize(offsetVec);
                    Debug.DrawLine(translation.Value,translation.Value+(steering),Color.red);
                    // }

                    // float3 acceleration = steering / steeringAgentParams.mass;
                    // float3 newVelocity = velocity.Value + acceleration * deltaTime;

                    var newVelocity = velocity.Value + steering;
                    if (math.length(newVelocity) > steeringAgentParams.maxSpeed)
                    {
                        newVelocity = math.normalize(newVelocity) * steeringAgentParams.maxSpeed;
                    }

                    chunkVelocities[i] = new Velocity { Value = newVelocity };
                    
                    // float3 newPos = translation.Value + newVelocity * deltaTime;
                    // chunkTranslations[i] = new Translation { Value = newPos };
                    // Debug.LogError(targetPos + " " + translation.Value + " " + newVelocity);
                }
            }
        }

    }
    }