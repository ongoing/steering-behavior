﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;
using System.Collections.Generic;
using Himeki.DOTS.UnitySteeringLib;
using UnityEngine;

[UpdateAfter(typeof(LeaderSystem))]
public class FormationSystem : SystemBase
{
    private EntityQuery m_LeaderQuery;


    protected override void OnCreate()
    {
        m_LeaderQuery = this.GetEntityQuery(
            ComponentType.ReadOnly<FormationLeader>(),
            ComponentType.ReadOnly<Translation>(),
            ComponentType.ReadOnly<Rotation>(),
            ComponentType.ReadOnly<Velocity>()
        );
    }

    protected override void OnUpdate()
    {
        var leaders = m_LeaderQuery.ToComponentDataArray<FormationLeader>(Allocator.TempJob);
        var leaderPositions = m_LeaderQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
        var leaderRotations = m_LeaderQuery.ToComponentDataArray<Rotation>(Allocator.TempJob);
        var leaderVelocitys = m_LeaderQuery.ToComponentDataArray<Velocity>(Allocator.TempJob);

        var formationGroups = new List<FormationGroup>();
        EntityManager.GetAllUniqueSharedComponentData(formationGroups);
        
        var leaderPosArray = new NativeArray<float3>(10,Allocator.TempJob);
        var leaderVelArray = new NativeArray<float3>(10,Allocator.TempJob);
        var leaderRotArray = new NativeArray<quaternion>(10,Allocator.TempJob);

        Entities
            // .WithAll<FormationLeader>()
            .ForEach((Entity entity, Translation translation, FormationLeader formationLeader, Rotation rotation,
                Velocity velocity) =>
            {
                leaderPosArray[formationLeader.Id] = translation.Value;
                leaderRotArray[formationLeader.Id] = rotation.Value;
                leaderVelArray[formationLeader.Id] = velocity.Value;
            })
            .Schedule();
        //.WithoutBurst().Run();
        
        Entities
            // .WithAll<FormationLeader>()
            // .WithChangeFilter<SteeringAgentParameters>()
            .ForEach((Entity entity, FormationSoldier group,Velocity velocity,ref SteeringAgentParameters steeringAgent,
                ref Translation translation,ref Rotation rotation) =>
            {
                var leaderPos = leaderPosArray[group.LeaderId];
                var leaderRot = leaderRotArray[group.LeaderId];
                var leaderVel = leaderVelArray[group.LeaderId];
                // var targetPosition = math.mul(leaderRot, steeringAgent.LocalPosition);
                // steeringAgent.WorldPosition = targetPosition + leaderPos;
                steeringAgent.LeaderRotation = leaderRot;
                steeringAgent.LeaderVelocity = leaderVel;
                steeringAgent.LeaderPosition = leaderPos;
                
                // translation.Value = steeringAgent.WorldPosition;
                // rotation.Value = leaderRot;

            })
            .Schedule();
            // .WithoutBurst().Run();

        leaderPosArray.Dispose(Dependency);
        leaderRotArray.Dispose(Dependency);
        leaderVelArray.Dispose(Dependency);
        // foreach (var formationGroup in formationGroups)
        // {
        //     if (formationGroup.LeaderId < 0)
        //         continue;
        //     for (int i = 0; i < leaders.Length; i++)
        //     {
        //         if (leaders[i].Id == formationGroup.LeaderId)
        //         {
        //             leaderPos = leaderPositions[i].Value;
        //             leaderRot = leaderRotations[i].Value;
        //             leaderVel = leaderVelocitys[i].Value;
        //             Debug.LogError(leaderPos);
        //         }
        //     }
        //     Entities
        //         .WithSharedComponentFilter(formationGroup)
        //         .ForEach((ref SteeringAgentParameters steeringAgent) =>
        //         {
        //             var targetPosition = math.mul(leaderRot, steeringAgent.TargetPosition);
        //             steeringAgent.TargetPosition = targetPosition + leaderPos;
        //             // steeringAgent.maxSpeed = math.length(leaderVel);
        //         }).Run();
        // }
        leaders.Dispose();
        leaderPositions.Dispose();
        leaderRotations.Dispose();
        leaderVelocitys.Dispose();
    }
}
