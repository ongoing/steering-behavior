using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine.Jobs;

namespace Himeki.DOTS.UnitySteeringLib
{
    [AlwaysSynchronizeSystem]
    // [DisableAutoCreation]
    [UpdateAfter(typeof(FormationSystem))]
    public class SteeringSystem : JobComponentSystem
    {
        private EntityQuery _agentsQuery;
        private EntityQuery _obstaclesQuery;

        protected override void OnCreate()
        {
            _agentsQuery = GetEntityQuery(ComponentType.ReadOnly<SteeringAgentParameters>());
            _obstaclesQuery = GetEntityQuery(ComponentType.ReadOnly<Obstacle>(), ComponentType.ReadOnly<Translation>());
        }

        protected override void OnDestroy()
        {
        }

        protected override JobHandle OnUpdate(JobHandle handle)
        {
            // this.GetComponentTypeHandle<VelocityVector>(true);
            var translationType = GetComponentTypeHandle<Translation>();
            var rotationType = GetComponentTypeHandle<Rotation>();
            var steeringAgentParametersType = GetComponentTypeHandle<SteeringAgentParameters>();
            var velocityType = GetComponentTypeHandle<Velocity>();
            // var targetType = GetComponentTypeHandle<TargetEntity>(true);
            var localToWorldType = GetComponentTypeHandle<LocalToWorld>(true);

            var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>(true);
            var velocityFromEntity = GetComponentDataFromEntity<Velocity>(true);

            var obstaclesTranslations = _obstaclesQuery.ToComponentDataArray<Translation>(Allocator.TempJob);
            var obstacles = _obstaclesQuery.ToComponentDataArray<Obstacle>(Allocator.TempJob);

            var steeringJob = new CalculateSteeringJob()
            {
                deltaTime = Time.DeltaTime,
                translationType = translationType,
                steeringAgentParametersType = steeringAgentParametersType,
                velocityType = velocityType,
                // targetType = targetType,
                localToWorldFromEntity = localToWorldFromEntity,
                velocityFromEntity = velocityFromEntity,
                obstaclesTranslations = obstaclesTranslations,
                obstacles = obstacles
            };

            var translJob = new UpdateEntityTranslationJob()
            {
                deltaTime = Time.DeltaTime,
                translationType = translationType,
                steeringAgentParametersType = steeringAgentParametersType,
                localToWorldType = localToWorldType,
                rotationType = rotationType,
                velocityType = velocityType,
            };

            // var calculateSteeringJobHandle = steeringJob.Schedule(_agentsQuery);
            // var updateTranslationJobHandle = translJob.Schedule(_agentsQuery, calculateSteeringJobHandle);
            steeringJob.Run(_agentsQuery);
            var updateTranslationJobHandle = translJob.Schedule(_agentsQuery);

            return updateTranslationJobHandle;
        }

    }

}