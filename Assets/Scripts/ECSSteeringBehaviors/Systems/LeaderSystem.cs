﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Himeki.DOTS.UnitySteeringLib
{
    public class LeaderSystem : SystemBase
    {
        public static float3 worldPoint = float3.zero;
        private static float turnTime = 2;

        protected override void OnUpdate()
        {
            // float3 fleeTarget = float3.zero;
            // var worldPoint = float3.zero;
            var changePos = false;
            if (Camera.main is { } && Input.GetMouseButtonUp(0))
            {
                float3 point = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                point -= new float3(0.5f, 0.5f, 0);
                point.z = point.y;
                point.y = 0;
                point = math.normalize(point);
                var v = point * 10;
                // Debug.LogError(v);

                var _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                //射线检测，相关检测信息保存到RaycastHit 结构中  
                if (Physics.Raycast(_ray, out var hit))
                {
                    worldPoint = hit.point; //Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    worldPoint.y = 0;
                    turnTime = 0.8f;
                    changePos = true;
                }

                // Debug.LogError(worldPoint);
            }

            var changeAngle = false;
            var up = new float3(0f, 1f, 0f);
            var right = new float3(100f, 0f, 0f);
            var dt = Time.DeltaTime;
            Entities
                .WithAll<FormationLeader>()
                .ForEach((Entity entity,ref FormationLeader formationLeader, ref Translation translation, ref Rotation rotation, ref Velocity velocity) =>
                {
                    var vel = worldPoint - translation.Value;
                    var dis = math.length(vel);
                    if (dis > 0.1f)
                    {
                        if (formationLeader.Id == 2)
                        {
                            vel = math.normalize(vel) * formationLeader.speed;
                            if (Vector3.Angle(vel, velocity.Value) > 90)
                            {
                                changeAngle = true;
                            }

                            velocity.Value = vel;
                            translation.Value += velocity.Value * dt;
                            rotation.Value = quaternion.LookRotation(velocity.Value, up);
                        }
                        else
                        {
                            if (changePos)
                            {
                                changePos = false;
                                vel = math.normalize(vel) * formationLeader.speed;
                                velocity.Value = vel;
                                translation.Value = translation.Value + velocity.Value * turnTime;
                                rotation.Value = quaternion.LookRotation(velocity.Value, up);
                                formationLeader.virtualPos += velocity.Value * dt;
                            }
                            else if (turnTime <= 0)
                            {
                                vel = math.normalize(vel) * formationLeader.speed;
                                velocity.Value = vel;
                                translation.Value += velocity.Value * dt;
                                rotation.Value = quaternion.LookRotation(velocity.Value, up);
                                formationLeader.virtualPos = translation.Value;
                            }
                            else
                            {
                                vel = math.normalize(vel) * formationLeader.speed;
                                velocity.Value = vel;
                                formationLeader.virtualPos += velocity.Value * dt;
                                // rotation.Value = quaternion.LookRotation(velocity.Value, up);
                            }
                            Debug.DrawLine(formationLeader.virtualPos,formationLeader.virtualPos+(vel),Color.red);

                        }
                    }

                }).WithoutBurst().Run();
            
            turnTime -= dt;

            if (changeAngle)
            {
                Entities
                    .WithAll<SteeringAgentParameters>()
                    .ForEach((Entity entity, ref SteeringAgentParameters agent) =>
                    {
                        agent.LocalPosition.x *= -1;
                        agent.LocalPosition.z *= -1;
                    }).Run();
            }
        }
    }

}